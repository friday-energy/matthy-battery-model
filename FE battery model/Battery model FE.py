# -*- coding: utf-8 -*-
"""
EMS system model using the FridayEnergy battery for individual customer optimisation

"""
import gurobipy as gp
import pandas as pd 
import numpy as np 
import matplotlib.pyplot as plt

"""
Import input data for the model
"""
input_data = pd.read_csv('FE model inputs.csv', sep = None, engine = 'python', index_col = 0)

parameter_data = pd.read_csv('FE model parameters.csv', sep = None, engine = 'python', index_col = 0)

#EMS - Friday energy demand and production profiles 

electricity_demand = input_data.Demand_load
PV_prod = input_data.PV_generation

"""
Parameters values
"""
#### Time-step
Delta_t = 1 #1 hour intervals
T = 8760 #number of time-slots in a year
Time_of_day = input_data.hour
Day_of_week = input_data.weekday

#### Price data
el_price = pd.Series(index = Day_of_week.index)
for t in range(T):
    if (Day_of_week[t] == 1 or Day_of_week[t] == 2 or Day_of_week[t] == 3 or Day_of_week[t] == 4 or Day_of_week[t] == 5) and (Time_of_day[t] == 7 or Time_of_day[t] == 8 or Time_of_day[t] == 9 or Time_of_day[t] == 10 or Time_of_day[t] == 11 or Time_of_day[t] == 12 or Time_of_day[t] == 13 or Time_of_day[t] == 14 or Time_of_day[t] == 15 or Time_of_day[t] == 16 or Time_of_day[t] == 17 or Time_of_day[t] == 18 or Time_of_day[t] == 19 or Time_of_day[t] == 20 or Time_of_day[t] == 21 or Time_of_day[t] == 22):
        el_price[t] = parameter_data.Value.Peak_price
    else:
        el_price[t] = parameter_data.Value.Offpeak_price
        
sde_price = parameter_data.Value.SDE #[euro/kWh]

base_price_own = parameter_data.Value.SDE_selfuse #[euro/kWh]

SDE_self = sde_price - base_price_own #[euro/kWh]

El_tax_t1 = parameter_data.Value.Tax_1 #[euro/kWh]
El_tax_t2 = parameter_data.Value.Tax_2 #[euro/kWh]
El_tax_t3 = parameter_data.Value.Tax_3 #[euro/kWh]

step1 = 10000
step2 = 50000
step3 = 10000000

p1 = step1 * El_tax_t1
p2 = ((step2 - step1) * El_tax_t2) + p1
p3 = ((step3 - step2) * El_tax_t3) + p2

peak_price = parameter_data.Value.Peakdemand_cost #[euro/peak kW]
transport_cost = parameter_data.Value.Transport_cost #[euro/kWh consumed]
capacity_cost = parameter_data.Value.Capacity_cost #[euro/kW contracted]

### Investment costs
PV_investment = parameter_data.Value.PV_investment #[euro/kWp]
Battery_investment = parameter_data.Value.Batt_investment #[euro/kWh]

Lifetime = 20 #[years]
Discount_rate = parameter_data.Value.Discount_rate
#PV config
PV_capacity = parameter_data.Value.PV_capacity #[kWp]

#### Limits on grid and max,min
Pgridmax = parameter_data.Value.GridConnection_limit #[kW]
Feedgridmax = parameter_data.Value.Feedin_limit #[kW]

#### Battery parameters

Batt_ratio = parameter_data.Value.Batt_capacitytopower_ratio #battery capacity to power ratio
eff_dis = np.sqrt(parameter_data.Value.Battery_efficiency) #battery discharging efficiency
eff_ch = np.sqrt(parameter_data.Value.Battery_efficiency) #battery charging efficiency

"""
Part 2: Create a model
"""
m = gp.Model()

"""
Part 3: Model variables
"""
######## Model Variables

Power_grid_to_user = m.addVars(T, name = 'Power_grid_to_user')
Power_grid_to_battery = m.addVars(T, name ='Power_grid_to_battery')
Power_battery_to_user = m.addVars(T, name = 'Power_battery_to_user')
Power_battery_to_grid = m.addVars(T, name = 'Power_battery_to_grid')
Power_PV_to_battery = m.addVars(T, name = 'PV_to_battery')
Power_PV_to_user = m.addVars(T, name = 'PV_to_user')
Power_PV_to_grid = m.addVars(T, name = 'PV-to_grid')
Battery_energy = m.addVars(T, name = 'Battery_energy')
El_price = m.addVars(T, name = 'Electricity_price')
PV_energy = m.addVars(T, name = 'PV_energy')
Power_from_grid = m.addVars(T, name = 'Power_from_grid')
Power_to_grid = m.addVars(T, name = 'Power_to_grid')
Grid_use = m.addVars(T, name = 'Grid_use')
Grid_abuse = m.addVars(T, name = 'Grid_abuse')
Battery_charge = m.addVars(T, name = 'Battery_charge')
Battery_discharge = m.addVars(T, name = 'Battery_discharge')
Battery_size = m.addVar(name = 'Battery_size')
MAX_grid_use = m.addVar(name = 'MAX_grid_use')
Taxes = m.addVar(name = 'Taxes')
a = m.addVar(name = 'a')
b = m.addVar(name = 'b')

"""
Part 4: Model Constraints
"""

######## Nonnegative variables 
m.addConstrs(Power_grid_to_user[t] >= 0 for t in range(T))
m.addConstrs(Power_grid_to_battery[t] >= 0 for t in range(T))
m.addConstrs(Power_battery_to_user[t] >= 0 for t in range(T))
m.addConstrs(Power_battery_to_grid[t] >= 0 for t in range(T))
m.addConstrs(Power_PV_to_battery[t] >= 0 for t in range(T))
m.addConstrs(Power_PV_to_user[t] >= 0 for t in range(T))
m.addConstrs(Power_PV_to_grid[t] >= 0 for t in range(T)) 
m.addConstrs(Grid_use[t] >= 0 for t in range(T))
m.addConstrs(Grid_abuse[t] >= 0 for t in range(T))

######## Power balance formulas
m.addConstrs(Power_battery_to_grid[t] + Power_PV_to_grid[t] == Power_to_grid[t] for t in range(T))
m.addConstrs(Power_grid_to_user[t] + Power_grid_to_battery[t] == Power_from_grid[t] for t in range(T))
m.addConstrs(Power_grid_to_battery[t] + Power_PV_to_battery[t] == Battery_charge[t] for t in range(T))
m.addConstrs(Power_battery_to_user[t] + Power_battery_to_grid[t] == Battery_discharge[t] for t in range(T))

m.addConstrs(Power_grid_to_user[t] + Power_battery_to_user[t] + Power_PV_to_user[t] == electricity_demand[t] for t in range(T))

m.addConstrs(Power_PV_to_battery[t] + Power_PV_to_user[t] + Power_PV_to_grid[t] == PV_energy[t] for t in range(T))
m.addConstrs(PV_energy[t] <= PV_prod[t] for t in range(T))

m.addConstr(Grid_use[0] == Power_grid_to_user[0] + Power_grid_to_battery[0])
m.addConstr(Grid_abuse[0] == Power_battery_to_grid[0] + Power_PV_to_grid[0])
m.addConstrs(Grid_use[t+1] == Grid_use[t] + Power_grid_to_user[t+1] + Power_grid_to_battery[t+1] for t in range(T-1)) 
m.addConstrs(Grid_abuse[t+1] == Grid_abuse[t] + Power_battery_to_grid[t+1] + Power_PV_to_grid[t+1] for t in range(T-1))

######## Battery SoC dynamics constraints
m.addConstr(Battery_energy[0] == 0.5*Battery_size - (Power_battery_to_user[0]/eff_dis) - (Power_battery_to_grid[0]/eff_dis) + (Power_PV_to_battery[0]*eff_ch) + (Power_grid_to_battery[0]*eff_ch))
m.addConstrs(Battery_energy[t+1] == Battery_energy[t] - (Power_battery_to_user[t+1]/eff_dis) - (Power_battery_to_grid[t+1]/eff_dis) + (Power_PV_to_battery[t+1]*eff_ch) + (Power_grid_to_battery[t+1]*eff_ch) for t in range(T-1))
#m.addConstrs(Battery_energy[t] == 0.5*Battery_size[0] for t in range(T) if t % 24 == 23)

######## SoC constraints 
m.addConstrs(Battery_energy[t] <= Battery_size for t in range(T))
m.addConstrs(Battery_energy[t] >= 0.2*Battery_size for t in range(T))

####### Maximum battery cycles allowed per year, determined from CycleLife/Lifetime
m.addConstr(gp.quicksum(Battery_discharge[t] for t in range(T)) <= Battery_size * 400)

######## Power boundaries
m.addConstrs(Power_battery_to_user[t] + Power_battery_to_grid[t] <= Battery_size/Batt_ratio for t in range(T))
m.addConstrs(Power_grid_to_battery[t] + Power_PV_to_battery[t] <= Battery_size/Batt_ratio for t in range(T))    
#m.addConstrs(Power_grid_to_user[t] + Power_grid_to_battery[t] <= Pgridmax for t in range(T))
m.addConstrs(Power_PV_to_grid[t] + Power_battery_to_grid[t] <= Feedgridmax for t in range(T))

####### Switch between Battery determinate modes
battery_mode = parameter_data.Value.Battery_mode

if battery_mode == 1:
    m.addConstr(Battery_size == parameter_data.Value.Battery_capacity)
else:
    m.addConstr(Battery_size >= 0)

#m.addConstr(Battery_size == 45)
######Costs

m.addConstrs(El_price[t] == el_price[t] for t in range(T))
m.addGenConstrPWL(Grid_use[8759], Taxes, [0, step1, step2, step3], [0, p1, p2, p3])

#### Peak grid use
m.addConstr(a == gp.max_(Power_from_grid))
m.addConstr(b == gp.max_(Power_to_grid))
m.addConstr(MAX_grid_use == gp.max_(a, b))

"""
Part 5: Calculating and setting Objective function
"""
def Tax(i):
    if i <= 10000:
        Tax = (i * El_tax_t1)
    elif 10000 <= i <= 50000:
        Tax = ((i - 10000) * El_tax_t2) + (10000 * El_tax_t1)
    else:
        Tax = ((i - 50000) * El_tax_t3) + (40000 * El_tax_t2) + (10000 * El_tax_t1)
    return(Tax)

### Annual cashflow
    
Bill = gp.quicksum(Power_from_grid[t] * El_price[t] for t in range(T)) - gp.quicksum(Power_battery_to_grid[t] * El_price[t] for t in range(T)) + Taxes
  
SDE_revenue = (Power_PV_to_battery.sum() * SDE_self) + (Power_PV_to_user.sum() * SDE_self) + (Power_PV_to_grid.sum() * sde_price) 

Net_annual = Bill - SDE_revenue + (MAX_grid_use * peak_price) + (Grid_use[8759] * transport_cost) # + ((MAX_grid_use * 1.15) * capacity_cost)

### LCOE

lcoe_calc1 = (((Battery_size * Battery_investment) + (PV_capacity * PV_investment) + Net_annual) /(1 + Discount_rate))

lcoe_calc11 = (((Battery_investment * Battery_size) + Net_annual) /((1 + Discount_rate)**11))

def lcoe_calc_top(i):
    return (Net_annual/((1 + Discount_rate)**i))

def lcoe_calc_bottom(i):
    return (electricity_demand.sum()/((1 + Discount_rate)**i))

LCOEpart = []
for i in range(2,11):
    LCOEpart.append(lcoe_calc_top(i))
    
LCOEpart2 = []
for i in range(12,Lifetime+1):
    LCOEpart.append(lcoe_calc_top(i))    

wut = sum(LCOEpart) + sum(LCOEpart2)
LCOE_numerator = wut + lcoe_calc1 + lcoe_calc11

LCOEpart2 = []
for i in range(1,Lifetime+1):
    LCOEpart2.append(lcoe_calc_bottom(i))
 
LCOE_denominator = sum(LCOEpart2)

LCOE = LCOE_numerator / LCOE_denominator

m.setObjective(LCOE, gp.GRB.MINIMIZE)
#m.setObjective(Net_annual, gp.GRB.MINIMIZE)

"""
Part 6: Define solving algorithm and parameter 
"""
m.setParam("Heuristics", 0.5)
m.setParam("BarConvTol", 1e-12)
m.setParam("FeasibilityTol", 1e-9)
m.setParam("Method", 2)
m.optimize()

"""
Part 7: Output model solution
""" 

######## Get the values of the decision variables

M = m.getAttr("X")
N = 16
P = np.reshape(M[0:140160], (N,T))
p_gen = np.asmatrix(P)

#### print values to console
print()
#print("p_gen:", p_gen)
print("PV used:", sum(P[9]))
print("Energy bought:", P[12,8759])
print("Energy sold:", P[13,8759])
print("Max grid use:", M[140161])
print("Battery capacity:", M[140160] )

#print("a:", M[140163])
#print("b:", M[140164])

print("LCOE:", LCOE.getValue())
print("Net annual cash flow:", Net_annual.getValue())


####create CSV file with model results
pd.DataFrame(np.transpose(P)).to_csv('FE battery model results.csv', header = ['Power_grid_to_user', 'Power_grid_to_battery', 'Power_battery_to_user', 'Power_battery_to_grid', 'PV_to_battery', 'PV_to_user', 'PV-to_grid', 'Battery_energy', 'Electricity_price', 'PV_energy', 'Power_from_grid', 'Power_to_grid', 'Grid_use', 'Grid_abuse', 'Battery_charge', 'Battery_discharge'])

